const nano = require('nano')('http://admin:adminpw@localhost:5984');

/**
 *
 * @param userId
 */
function getDataSources() {
    const data_sources = nano.use("data_sources")
    data_sources.list().then((body) =>{
        body.rows.forEach((doc) => {
            console.log(doc.doc)
        })
    })
}

function addDataSource(dataSource, id = ""){
    const data_sources = nano.use("data_sources")

    if (id !== null){
        data_sources.insert(dataSource, id).then((body) => {
            console.log(body)
        })
    }

}

function addCifarDataSources() {
    const basepath = "C:\\Users\\micha\\projects\\Conductor\\sample_data\\cifar10\\cifar-10-batches-py\\batch_"


    for (let i = 0; i < 5; i++){
        let path = basepath + (i + 1)
        let dataSource = {
            name: "cifar batch" + (i +1),
            trains: [],
            type: "image_folder",
            path: basepath + (i+ 1),
        }
        addDataSource(dataSource, "cifar_batch_" + (i + 1))

        console.log(path)

    }
}

export default {addDataSource, getDataSources}