const nano = require('nano')('http://admin:adminpw@localhost:5984');

/**
 * Set up databases for the station
 */

//Database for local trains
nano.db.destroy("trains").then( (response) => {
    return nano.db.create("trains").then((data) => {
        console.log(data)
    }).catch((err) => {
        console.log(err)
    })
}).catch((err) => {
    console.log(err)
    return nano.db.create("trains").then((data) => {
        console.log(data)
    }).catch((err) => {
        console.log(err)
    })
})

//Database for registered data sources
nano.db.destroy("data_sources").then( (response) => {
    return nano.db.create("data_sources").then((data) => {
        console.log(data)
    }).catch((err) => {
        console.log(err)
    })
}).catch((err) => {
    console.log(err)
    return nano.db.create("data_sources").then((data) => {
        console.log(data)
    }).catch((err) => {
        console.log(err)
    })
})


//Database for data providers created by users
nano.db.destroy("data_providers").then( (response) => {
    return nano.db.create("data_providers").then((data) => {
        console.log(data)
    }).catch((err) => {
        console.log(err)
    })
}).catch((err) => {
    console.log(err)
    return nano.db.create("data_providers").then((data) => {
        console.log(data)
    }).catch((err) => {
        console.log(err)
    })
})

// Station User database
nano.db.destroy("station_users").then( (response) => {
    return nano.db.create("station_users").then((data) => {
        console.log(data)
    }).catch((err) => {
        console.log(err)
    })
}).catch((err) => {
    console.log(err)
    return nano.db.create("station_users").then((data) => {
        console.log(data)
    }).catch((err) => {
        console.log(err)
    })
})


