const ipfsClient = require('ipfs-http-client');
const fs = require("fs");
const path = require('path');
const {globSource} = require("ipfs-http-client");
//TODO set address of ipfs node based on configuration or env vars
const ipfs = ipfsClient('http://localhost:5001')

const globSourceOptions = {
    recursive: true
};

/**
 * Add a file system path to ipfs storage, processes both files and directories
 *
 * @param path path to file or directory
 * @returns {Promise<void>}
 */
async function addPathIpfs(path){
    let CID;
    let result = await ipfs.add( globSource(path, globSourceOptions))
    // ipfs.add( globSource(path, globSourceOptions)
    // ).then((res) => {
    //     CID = String(res.cid)
    //     console.log(CID)
    //     return CID
    // }).catch((err) =>{
    //     console.log(err)
    // })
    return result

}


/**
 * Get a file from ipfs storage based on the given content id and store it at the given path
 *
 * @param cid ipfs CID identifying the file
 * @param path path to save the file to
 * @returns {Promise<void>}
 */
async function getIpfsFile(cid, path) {
    const ipfsFile = fs.createWriteStream(path);
    for await (const chunk of ipfs.cat(cid)){
        console.log(chunk)
        ipfsFile.write(chunk)
    }
}

/**
 * Get a directory from ipfs storage based on its CID and store it in the target directory.
 * Creates the target directory if it does not exist.
 *
 * @param cid IPFS CID pointing to the directory
 * @param targetDir path to the directory in which the files should be saved
 * @returns {Promise<void>}
 */
async function getIpfsDir(cid, targetDir){

    // check if the target directory exists otherwise create it
    fs.lstat(targetDir, (err, stats) => {
        if (err){
            fs.mkdir(targetDir, (direrr) =>{
                console.log(direrr)
            })
        }
        else if (stats.isFile()){
            console.log("Target path contains an already existing file")
        }
    });

    for await (const file of ipfs.get(cid)){
        console.log(file.path)
        let fileName = path.parse(file.path).base;
        // Skip files if they dont have content
        if (!file.content) continue;
        let f = fs.createWriteStream(path.resolve(targetDir, fileName))
        for await (const chunk of file.content){
            f.write(chunk)
        }
    }
}

module.exports = {addPathIpfs, getIpfsFile, getIpfsDir}
