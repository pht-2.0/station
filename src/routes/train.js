var express = require('express');
var router = express.Router();
const {addPathIpfs, getIpfsFile, getIpfsDir} = require("../ipfs/ipfs_client.js")
const nano = require('nano')('http://admin:adminpw@localhost:5984');


/**
 * Get all the trains in the local train data base
 */
router.get("/list", async function (req, res) {
    //TODO authorization + only return user associated trains
    const train_db = nano.use("trains")

    await train_db.list(function (err, body) {
        console.log("Fetching trains from db")
        let trains = [];
        if (!err){
            body.rows.forEach((doc) => {
                trains.push(doc)
                console.log(doc)
            })
            res.json(trains)
        }
    })

});

router.post("/submit", function (req, res) {

});

/**
 * Process a train submission containing a train directory, a train name and the submitter stored the train files in
 * ipfs storage and other relevant data in the local train database
 */
router.post("/submitProposal",async function (req, res) {
    // Add the given directory to ipfs
    let trainDir = req.body.trainDir
    let cid;
    // Add train directory to ipfs storage
    // TODO accept zipped train directory
    addPathIpfs(trainDir).then((resp) => {
        // console.log(resp)
        cid = String(resp.cid)
        return cid;
    }).then( (cid) => {
        // Add the train to the local database with the created IPFS CID
        const train_db = nano.use("trains")
        let db_train = req.body;
        console.log(cid)
        db_train.cid = cid
        train_db.insert(db_train).then( (resp) => {
            console.log(resp)
            res.json(resp.id)
        });
    }).catch((err) => {
        console.log(err)
    })
    // Add the train with hash to to the station database
});

router.post("/run", async (req, resp) => {
    let trainDir = req.body.trainDir;
    let dataDir = req.body.dataDir;
    let conductor = req.body.conductor;
    let type = req.body.trainDirType;

    if(type === "ipfs"){
        getIpfsDir(req.body.cid, trainDir)
    }

})


module.exports = router;