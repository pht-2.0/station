'use strict';

// Fabric smart contract classes
const { Contract, Context } = require('fabric-contract-api');

const Train = require("./train.js")
const TrainList = require("./trainList.js")

class TrainContext extends Context {
    constructor() {
        super();
        this.trainList = new TrainList(this)
    }
}

/**
 * Basic train contract based on the fabric contract api
 */
class TrainContract extends Contract {

    constructor() {
        super("pht.train");
    }

    createContext() {
        return new TrainContext();
    }

    /**
     * Create a train proposal and submit it to the blockchain
     *
     * @param ctx
     * @param submitter
     * @param trainId
     * @param submissionTime
     * @param proposalIpfsHash
     * @returns {Promise<Train>}
     */
    async submit(ctx, submitter, trainId, submissionTime, proposalIpfsHash){
        let train = Train.createInstance(submitter, trainId, submissionTime, proposalIpfsHash);
        train.setSubmitted();
        await ctx.trainList.addTrain(train);
        return train
    }

    /**
     * A station publishes a transaction indicating that the train is currently being executed
     * @param ctx the transaction context
     * @param submitter original submitter of the train to form key for identifying the train
     * @param trainId  id of the train for making the unique key
     * @returns {Promise<*>}
     */
    async execute(ctx, submitter, trainId){
        let trainKey = Train.makeKey([submitter, trainId])
        let train = await ctx.trainList.getTrain(trainKey)

        if (train.isRunning()){
            throw new Error("Train is currently being executed")
        }

        if (train.isFinished()){
            throw new Error("Train is already finished")
        }
        // Set the train to running state
        if (train.isIdle()){
            train.setRunning()
        }
        else if (train.isSubmitted()){
            train.setRunning()
        }

        await ctx.trainList.updateTrain(train)

        return train
    }

    /**
     * Transaction that is made by the station once it is finished, includes the CID for the train directory stored in
     * IPFS
     *
     * @param ctx
     * @param submitter
     * @param trainId
     * @param ipfsResultsCID
     * @returns {Promise<*>}
     */
    async stationFinished(ctx, submitter, trainId, ipfsResultsCID){
        let trainKey = Train.makeKey([submitter, trainId])
        let train = await ctx.trainList.getTrain(trainKey)

        if (!train.isRunning()){
            throw new Error("Train state mismatch cannot execute done transactions on not running train")
        }
        else {
            train.setIdle();
            train.setTrain(ipfsResultsCID);
        }
        await ctx.trainList.updateTrain(train);
        return train;

    }

    /**
     * When the train is finished according to its configuration in the train_config.yaml file update the state
     *
     * @param ctx
     * @param submitter
     * @param trainId
     * @param ipfsResultsCID
     * @returns {Promise<*>}
     */
    async trainFinished(ctx, submitter, trainId, ipfsResultsCID){
        let trainKey = Train.makeKey([submitter, trainId])
        let train = await ctx.trainList.getTrain(trainKey)

        if (!train.isRunning()){
            throw new Error("Train state mismatch cannot execute train finished transactions on not running train")
        }
        else {
            train.setFinished();
            train.setTrain(ipfsResultsCID);
        }
        await ctx.trainList.updateTrain(train);
        return train;

    }


}