'use strict';

const StateList = require('../ledger-api/statelist');
const Train = require('./train')

class TrainList extends StateList{
    constructor(ctx) {
        super(ctx, 'pht.pht-main.trainlist');
        this.use(Train)
    }

    async addTrain(train){
        return this.addState(train)
    }

    async getTrain(trainKey){
        return this.getState(trainKey)
    }

    async updateTrain(train){
        return this.updateState(train)
    }
}

module.exports = TrainList;