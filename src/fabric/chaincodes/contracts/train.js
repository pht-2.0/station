
'use strict';

const State = require('../ledger-api/state');

// enumerate train states

const train_states = {
    "SUBMITTED": 1,
    "RUNNING": 2,
    "IDLE": 3,
    "FINISHED": 4,
}

class Train extends State {
    constructor(obj) {
        super(Train.getClass(), [obj.submitter, obj.trainId]);
        Object.assign(this, obj);
    }

    /**
     * Basic getters and setters
     */
    getSubmitter() {
        return this.submitter;
    }

    setSubmitter(newSubmitter) {
        this.submitter = newSubmitter;
    }

    getProposal(){
        return this.trainProposalCID
    }

    setProposal(trainProposalCID){
        this.trainProposalCID = trainProposalCID
    }

    getParticipants(){
        return this.participants
    }

    setParticipants(participants){
        this.participants = participants
    }

    getTrain(){
        return this.trainCID
    }

    setTrain(trainCID){
        this.trainCID = trainCID
    }

    setIpfs(cid){
        this.ipfsCID = cid
    }

    /**
     * Useful methods to encapsulate train states
     */
    setSubmitted() {
        this.currentState = train_states.SUBMITTED;
    }

    setRunning() {
        this.currentState = train_states.RUNNING;
    }

    setFinished() {
        this.currentState = train_states.FINISHED;
    }

    setIdle() {
        this.currentState = train_states.IDLE;
    }

    isSubmitted() {
        return this.currentState === train_states.SUBMITTED;
    }

    isRunning() {
        return this.currentState === train_states.RUNNING;
    }

    isFinished() {
        return this.currentState === train_states.FINISHED;
    }

    isIdle() {
        return this.currentState === train_states.IDLE
    }

    static fromBuffer(buffer) {
        return Train.deserialize(buffer);
    }

    toBuffer() {
        return Buffer.from(JSON.stringify(this));
    }

    /**
     * Deserialize a state data to commercial paper
     * @param {Buffer} data to form back into the object
     */
    static deserialize(data) {
        return State.deserializeClass(data, Train);
    }

    /**
     * Factory method to create a commercial paper object
     */
    static createInstance(submitter, trainId, submissionTime, trainProposalCID, trainCID = null,
                          participants = []) {
        return new Train({ submitter, trainId, submissionTime, trainProposalCID, trainCID, participants});
    }

    static getClass() {
        return 'pht.pht-main.train';
    }
}

module.exports = Train;