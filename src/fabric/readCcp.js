import {readFileSync} from "fs";
const yaml = require('js-yaml');


function readCommonConnectionProfile(path) {
    const data = readFileSync(path);

    const connectionProfile = yaml.safeLoad(data);
}

module.exports = readCommonConnectionProfile;

