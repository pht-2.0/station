# PHT Station  
Node js application that interacts with a hyperledger fabric distributed ledger and local
resources to manage Trains.  
Follow the [hyperledger fabric tutorials](https://hyperledger-fabric.readthedocs.io/en/release-2.2/tutorials.html)
to get familiar with the distributed ledger.

## Test Station
Run all services required for development using docker compose, in this projects root directory run:
```bash
docker-compose up
```
This will setup basic certificates for using the fabric network using the fabric-ca
as well as run containerized versions of following services:
- `localhost:5984`: CouchDB as the ledger state and general station data base
- `localhost:9443`: IBM Fhir server
- `localhost:8080`: ipfs http gateway
- `localhost:4001`: ipfs swarm port 
- `localhost:5001`: ipfs api port

## Fabric networks

### Install Fabric Binaries and Samples

```bash
cd test
curl -sSL https://bit.ly/2ysbOFE | bash -s
```

### Add fabric binaries to path
For interacting with the fabric binaries they should be added to the `PATH` variable.  
In the previously created `fabric-samples` directory run:
```bash
export PATH=$PATH:${PWD}/bin
```

### Test Network

Create test fabric network to test chaincode and other functionalities locally. Based on the 
[fabric test network](https://hyperledger-fabric.readthedocs.io/en/latest/test_network.html).  
To spin up a test network containing two orgs each with a CA and one peer and create a channel with those orgs, run
the following commands
```bash
cd test-network
./network.sh up createChannel -ca -s couchdb
```

Removes previously existing/running containers and cleans up generated files for starting a clean 
network. 

```bash
./network.sh down
```

### Monitoring the docker network
To monitor all containers participating in the network run in a separate terminal
```bash
./monitordocker.sh net_test
```
The first argument is the docker network name that should be monitored




