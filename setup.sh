#!/bin/bash

FABRIC_BIN=${PWD}/test/fabric-test-network/bin

mkdir -p organization/peerOrganizations/station.example.com/
export FABRIC_CA_CLIENT_HOME=${PWD}/organization/peerOrganization/station.example.com/

echo "Add fabric binaries to path"
export PATH=$FABRIC_BIN:$PATH

set -x
echo "Enroll CA admin"
fabric-ca-client enroll -u https://admin:adminpw@localhost:7054 --caname station_ca --tls.certfiles ${PWD}/organization/fabric-ca/station/tls-cert.pem

echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-7054-station_ca.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-7054-station_ca.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-7054-station_ca.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-7054-station_ca.pem
    OrganizationalUnitIdentifier: orderer' >${PWD}/organization/peerOrganizations/station.example.com/msp/config.yaml

echo "register peer0"
set -x
fabric-ca-client register --caname station_ca --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organization/fabric-ca/station/tls-cert.pem
{ set +x; } 2>/dev/null